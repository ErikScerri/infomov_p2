//-------------------------------------------------------------------------
// Simple back-propagation neural network example
// 2017 - Bobby Anguelov
// MIT license: https://opensource.org/licenses/MIT
//-------------------------------------------------------------------------
// A simple neural network supporting only a single hidden layer

#include "avx_math.h"

namespace Tmpl8 {

#define ROUNDUP(v,r) (v + (r - 1)) & ~(r - 1)

struct TrainingEntry
{
	float inputs[INPUTSIZE];
	int expected[NUMOUTPUT];
};

struct TrainingSet
{
	TrainingEntry* entry;
	int size;
};

struct TrainingData
{
	TrainingData( int t, int g, int v )
	{
		trainingSet.entry = new TrainingEntry[t];
		trainingSet.size = t;
		generalizationSet.entry = new TrainingEntry[g];
		generalizationSet.size = g;
		validationSet.entry = new TrainingEntry[v];
		validationSet.size = v;
	}
	TrainingSet trainingSet;
	TrainingSet generalizationSet;
	TrainingSet validationSet;
};

class Network
{
	friend class NetworkTrainer;
	inline float GetOutputErrorGradient( float desiredValue, float outputValue ) const { return outputValue * (1.0f - outputValue) * (desiredValue - outputValue); }
	//int GetInputHiddenWeightIndex(int inputIdx, int hiddenIdx) const { return inputIdx * (NUMHIDDEN + 1) + hiddenIdx; }
	int GetInputHiddenWeightIndex(int inputIdx, int hiddenIdx) const { return inputIdx + hiddenIdx * (ROUNDUP(INPUTSIZE + 1, 8)); }
	//int GetHiddenOutputWeightIndex(int hiddenIdx, int outputIdx) const { return hiddenIdx * NUMOUTPUT + outputIdx; }
	int GetHiddenOutputWeightIndex(int hiddenIdx, int outputIdx) const { return hiddenIdx + outputIdx * (ROUNDUP(NUMHIDDEN + 1, 8)); }

	inline static int ClampOutputValue(float x) { if (x < 0.1f) return 0; else if (x > 0.9f) return 1; else return -1; }
	inline static __m256i ClampOutputValue8(__m256 x)
	{
		__m256 zeroNines8 = _mm256_set1_ps(0.9f); // Vector of 0.9s.
		__m256 zeroOnes8 = _mm256_set1_ps(0.1f); // Vector of 0.1s.
		__m256 minusOnes8 = _mm256_set1_ps(-1.0f); // Vector of -1s.
		__m256 ones8 = _mm256_set1_ps(1.0f); // Vector of 1s.

		__m256 lessThanMask = _mm256_cmp_ps(x, zeroOnes8, 13); // Returns 0s if the < comparison of the input and 0.1 is true, 1s otherwise. (this is actually a >= comparison)
		__m256 greaterThanMask = _mm256_cmp_ps(x, zeroNines8, 14); // Returns 1s if the > comparison of the input and 0.9 is true, 0s otherwise.
		__m256 xorMask = _mm256_xor_ps(lessThanMask, greaterThanMask); // XOR the less than mask and greater than mask to get the positions where neither are true (i.e. the positions of the -1s).

		__m256 minusOnesResult = _mm256_and_ps(xorMask, minusOnes8); // Vector with -1s where they should be and 0s otherwise.
		__m256 onesResult = _mm256_and_ps(greaterThanMask, ones8); // Vector with 1s where they should be and 0s otherwise.
		__m256 finalResult = _mm256_add_ps(minusOnesResult, onesResult); // Add the two above together to get the final result.

		return _mm256_cvttps_epi32(finalResult); // Cast the final result to a vector of integers.
	}

	inline static float SigmoidActivationFunction(float x) { return 1.0f / (1.0f + expf(-x)); }
	inline static __m256 SigmoidActivationFunction8(__m256 x)
	{ 
		__m256 negX = _mm256_mul_ps(_mm256_set1_ps(-1.0f), x);
		__m256 expX = _mm256_add_ps(_mm256_set1_ps(1.0f), _es256_exp_ps(negX));
		return _mm256_rcp_ps(expX);
	}

public:
	Network();
	const int* Evaluate( const float* input );
	void Train( const TrainingData& trainingData );
	const float* GetInputHiddenWeights() const { return weightsInputHidden; }
	const float* GetHiddenOutputWeights() const { return weightsHiddenOutput; }
	void LoadWeights( const float* weights );
	void SaveWeights( float* weights );
	void InitializeNetwork();
	void InitializeWeights();
	float GetHiddenErrorGradient( int hiddenIdx ) const;
	void RunEpoch( const TrainingSet& trainingSet );
	void Backpropagate( const int* expectedOutputs );
	void UpdateWeights();
	void GetSetAccuracyAndMSE( const TrainingSet& trainingSet, float& accuracy, float& mse );
private:
	// neural net data
	//float inputNeurons[INPUTSIZE + 1];
	union { float inputNeurons[INPUTSIZE + 1]; __m256 inputNeurons8[(ROUNDUP(INPUTSIZE + 1, 8)) / 8]; };
	//float hiddenNeurons[NUMHIDDEN + 1];
	union { float hiddenNeurons[NUMHIDDEN + 1]; __m256 hiddenNeurons8[(ROUNDUP(NUMHIDDEN + 1, 8)) / 8]; };
public:
	union { float outputNeurons[NUMOUTPUT]; __m256 outputNeurons8[(ROUNDUP(NUMOUTPUT, 8)) / 8]; };
private:
	//int clampedOutputs[NUMOUTPUT];
	union { int clampedOutputs[NUMOUTPUT]; __m256i clampedOutputs8[(ROUNDUP(NUMOUTPUT, 8)) / 8]; };
	// float* weightsInputHidden;
	union { float* weightsInputHidden; __m256* weightsInputHidden8; };
	//float* weightsHiddenOutput;
	union { float* weightsHiddenOutput; __m256* weightsHiddenOutput8; };
	// training data
	//float*   deltaInputHidden;				// delta for input hidden layer
	union { float* deltaInputHidden; __m256* deltaInputHidden8; };
	//float*   deltaHiddenOutput;				// delta for hidden output layer
	union { float* deltaHiddenOutput; __m256* deltaHiddenOutput8; };
	float*   errorGradientsHidden;			// error gradients for the hidden layer
	float*   errorGradientsOutput;			// error gradients for the outputs
	int      currentEpoch;					// epoch counter
	float    trainingSetAccuracy;
	float    validationSetAccuracy;
	float    generalizationSetAccuracy;
	float    trainingSetMSE;
	float    validationSetMSE;
	float    generalizationSetMSE;
	unsigned long long totalCycles;
	unsigned long long methodCalls;
};

} // namespace Tmpl8